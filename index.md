---
layout: home
title: "home"
---

irish-based teacher and researcher in dublin. currently a teaching fellow at university college dublin where i primarily assist with the msc space science & technology module. 

i was awarded the john & pat hume doctoral scholarship in maynooth university, where i was a member of the [star and formation group](https://www.maynoothuniversity.ie/experimental-physics/star-and-planet-formation) as well as the [centre for astronomy and space science](https://www.maynoothuniversity.ie/casm). my interest is the evolution of young stellar objects, particularly the phenomenon of astrophysical jets from weirder objects. lately this has focused on the extreme end of the stellar spectrum, such as the irradiated [orion proplyds](https://esahubble.org/news/heic0917/) and sub-stellar objects like [brown dwarfs](https://phys.org/news/2017-05-weight-brown-dwarf-parsec-scale-jet.html).

i also enjoy (very amateur) photography, and one day i hope to get a decent camera. anything i'm proud of, i've put up on my [imgbb](https://akirwan.imgbb.com/albums) site, though it is in dire need of an update.
