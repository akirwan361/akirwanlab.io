---
layout: post
title: "a stellar sprinkler"
---

happy to announce another exciting paper! using the amazing muse instrument on the vlt, we observed a fascinating object called a `proplyd` (PROto-PLanetarY Disk) in the orion nebula known as 244-440 (or v* v2423 ori if you search on simbad). proplyds often look like bright bubbles or globules because they are being bombarded by ionizing radiation from the massive stars in the region. there are some beautiful photos on the [wikipedia](https://en.wikipedia.org/wiki/Proplyd) page to give you an idea.

this was observed with hubble previously but the pixel binning resulted in a reduced resolution so a lot of the detail of the object was lost. our muse images are a bit higher resolution and were focused specifically on this object (the hubble was taking large field surveys) so we see a ton of cool stuff -- most notably a highly curved, nearly `s`-shaped jet. our findings are also exciting because the shape of the jet appears to suggest one of two things: that this is not only a binary system, but that the jet may be driven by the smaller companion in the system; or that strong radiation and winds from other stars are bending the outflow. or maybe a combination of the two!

a free-access version of the paper can be found on the [a&a website](https://doi.org/10.1051/0004-6361/202245428). and the results were so interesting that we even got a picture-of-the-week from the european southern observatory! check it out [here](https://www.eso.org/public/images/potw2316a/)!
