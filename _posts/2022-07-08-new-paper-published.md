---
layout: post
title: "new paper published"
---

it took a while but i'm proud to finally have a publication under my belt. a study of the herbig ae star hd163296 showcases the complex and unique structure of the outflow system. we use `wiggle-ology` to explore how a close-in companion could influence the shape of the jet, ultimately finding that it is unlikely a companion greater than a tenth of a stellar mass exists at distances between 1 and 35 au from the source. most notably we find that the jet has an intrinsic emission asymmetry likely connected to shock processes in the knots themselves, and this falsely presents as a wandering of the jet axis (or `wiggle`). this also puts some limits on precessing jet models, suggesting that they are probably not the best tool for detecting planets because they're just too small and too far away compared to a companion star. 

a free-access version of the paper can be found on the [a&a website](https://doi.org/10.1051/0004-6361/202142862).
