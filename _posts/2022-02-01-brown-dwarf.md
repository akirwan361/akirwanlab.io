---
layout: post
title: "an ultra-low luminosity jet"
---

recently, i was awarded observing time on the [vlt/muse](https://www.eso.org/public/teles-instr/paranal-observatory/vlt/vlt-instr/muse/) instrument in chile, making us the first irish-based research group to be awarded muse time since ireland joined the european southern observatory in october, 2018. i requested time for two very unique brown dwarfs and their jets, one of which (mayrit 1802188 or eso-ha 1674) was only ever detected with spectroscopy, so there was very little spatial information available. we're working on a paper for this, but it is quite exciting these are the first images of this jet ever seen. not only that, but it is lowest luminosity jet so far detected, and we see so much of it because it is being externally irradiated. and since it's muse, we get an unprecedented amount of high resolution spatial detail across a wide range of velocities, ionization, and density. 
