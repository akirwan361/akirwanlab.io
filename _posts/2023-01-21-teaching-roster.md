---
layout: post
title: "final sememester"
---

both happy and sad to say that this is my final semester at the university: my thesis is on-track for submission before the end of february and i'll be crossing my fingers for a lovely post-doc position. what better way to close out the semester, though, than by teaching my first lecture course.

i've always had teaching duties, but these have been lab demonstrations and tutorial sessions. but this semester i get to teach the mathematical portion of the quantum mechanics module for experimental physics. exciting!
